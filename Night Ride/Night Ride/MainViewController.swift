import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var loadingProgressView: UIProgressView!
    @IBOutlet weak var raceButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var scoresButton: UIButton!
    @IBOutlet weak var leftFlashImageView: UIImageView!
    @IBOutlet weak var rightFlashImageView: UIImageView!
    
    var sound = Sound()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBeautyScreen()

        self.createFlash()
    }
    
    @IBAction func raceButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        guard let race = self.storyboard?.instantiateViewController(withIdentifier: "RaceViewController") as? RaceViewController else { return }
        
        self.navigationController?.pushViewController(race, animated: true)
    }
    
    @IBAction func settingsButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        guard let settings = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else { return }
        
        self.navigationController?.pushViewController(settings, animated: true)
    }
    
    @IBAction func scoresButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        guard let scores = self.storyboard?.instantiateViewController(withIdentifier: "ScoresViewController") as? ScoresViewController else { return }
        
        self.navigationController?.pushViewController(scores, animated: true)
    }
    
    @objc func updateProgressView() {
        if self.loadingProgressView.progress != 1 {
            self.loadingProgressView.progress += 1 / 100
        } else {
            self.raceButton.isHidden = false
            self.settingsButton.isHidden = false
            self.scoresButton.isHidden = false
            UIView.animate(withDuration: 0.4, animations: {
                self.raceButton.alpha = 1;
                self.loadingProgressView.alpha = 0;
                self.settingsButton.alpha = 1;
                self.scoresButton.alpha = 1
                self.raceButton.setRadiusWithShadow(radius: 50)
            })
        }
    }
    
    func createFlash() {
        UIView.animate(withDuration: 0.3) {
            self.leftFlashImageView.alpha = 0.15
            self.leftFlashImageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.rightFlashImageView.alpha = 0.1
            self.rightFlashImageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        } completion: { (_) in
            self.doubleFlash()
        }
    }
    
    func doubleFlash() {
        UIView.animate(withDuration: 0.3) {
            self.leftFlashImageView.alpha = 0
            self.leftFlashImageView.transform = .identity
            self.rightFlashImageView.alpha = 0
            self.leftFlashImageView.transform = .identity
        }
        self.createFlash()
    }
    
    func makeBeautyScreen() {
        Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateProgressView), userInfo: nil, repeats: true)
        
        self.loadingProgressView.setProgress  (0, animated: false)
        self.raceButton.isHidden = true
        self.raceButton.alpha = 0
        self.settingsButton.isHidden = true
        self.settingsButton.alpha = 0
        self.scoresButton.isHidden = true
        self.scoresButton.alpha = 0
    }
    
}

