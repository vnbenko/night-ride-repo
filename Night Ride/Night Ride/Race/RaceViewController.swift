import UIKit

class RaceViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var mainRoadView: UIView!
    @IBOutlet weak var roadAnimationView: UIView!
    @IBOutlet weak var leftRoadSide: UIView!
    @IBOutlet weak var rightRoadSide: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var rightRoadStripView: UIView!
    @IBOutlet weak var leftRoadStripView: UIView!
    
    // MARK: - let
    
    let raceBanner = UIImageView()
    let car = UIImageView()
    let traffic = UIImageView()
    let item = UIImageView()
    let leftTurnSignal = UIImageView()
    let rightTurnSignal = UIImageView()
    let frontLight = UIImageView()
    let rearLight = UIImageView()
    let road = UIImageView()
    let secondRoad = UIImageView()
    
    // MARK: - var
    
    
    var score = 0
    var carSpeed: Double = 0
    var startAnimation = true
    var lightOn = false
    var carModel = SettingsManager.shared.getCar()
    var timer = Timer()
    var sound = Sound()
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initGame()
        
    }
    
    // MARK: - IBAction
    @IBAction func handleCar(_ recognizer: UITapGestureRecognizer) {
        if self.car.frame.origin.x == self.mainRoadView.frame.width - self.car.frame.width - self.mainRoadView.frame.width / 6 {
            self.createTurnSignal(signal: self.leftTurnSignal)
            
            UIView.animate(withDuration: 0.3, animations: {
                self.car.frame.origin.x = self.mainRoadView.frame.width - (self.mainRoadView.frame.width / 6) * 5
                self.car.transform = CGAffineTransform(rotationAngle: .pi / 6)
            }) { (_) in
                self.leftTurnSignal.removeFromSuperview()
            }
            
            self.car.transform = .identity
        } else {
            self.createTurnSignal(signal: self.rightTurnSignal)
            
            UIView.animate(withDuration: 0.3, animations:  {
                self.car.frame.origin.x = self.mainRoadView.frame.width - self.car.frame.width - self.mainRoadView.frame.width / 6
                self.car.transform = CGAffineTransform(rotationAngle: .pi * 1.83)
            }) { (_) in
                self.rightTurnSignal.removeFromSuperview()
            }
            
            self.car.transform = .identity
        }
    }
    
    // MARK: Flow func
    
    func createPlayerCar() {
        let size = (width: self.leftRoadStripView.frame.width, height: self.leftRoadStripView.frame.width * 2)
        let position = (x: self.mainRoadView.frame.width / 6, y: self.mainRoadView.frame.height - size.height * 1.5)
        
        self.car.image = self.carModel
        self.car.frame = CGRect(x: position.x, y: position.y, width: size.width, height: size.height)
        self.car.setRadiusWithShadow(radius: self.car.frame.height / 3, color: UIColor.yellow.cgColor)
        self.car.contentMode = .scaleAspectFit
        self.mainRoadView.addSubview(self.car)
    }
    
    @objc func createTraffic() {
        if self.startAnimation {
            let size = self.car.frame.size
            let leftPosition = self.mainRoadView.frame.width / 6
            let rightPosition = self.mainRoadView.frame.width - size.width - self.mainRoadView.frame.width / 6
            let carPosition = [leftPosition, rightPosition]
            let trafficModel = SettingsManager.shared.getTrafficArray()
            self.traffic.image = trafficModel.randomElement()
            
            self.traffic.frame = CGRect(x: 0, y: -size.height, width: size.width, height: size.height)
            self.traffic.frame.origin.x = carPosition[Int.random(in: 0...1)]
            self.traffic.contentMode = .scaleAspectFit
            self.traffic.setRadiusWithShadow(radius: car.frame.height / 3, color: Constants.color[Int.random(in: 0...8)])
            self.mainRoadView.addSubview(self.traffic)
            
            UIView.animate(withDuration: 1.5 - carSpeed, delay: 0, options: .curveLinear, animations: {
                self.traffic.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.traffic.frame.origin.y = self.mainRoadView.frame.height
            }) { (_) in
                self.traffic.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.traffic.removeFromSuperview()
                self.createTraffic()
            }
        }
    }
    
    
    @objc func createAbilityItem() {
        if startAnimation {
            let size = self.car.frame.size
            let leftPosition = self.mainRoadView.frame.width / 6
            let rightPosition = self.mainRoadView.frame.width - size.width - self.mainRoadView.frame.width / 6
            let itemPosition = [leftPosition, rightPosition]
            
            self.item.image = UIImage(named: "bonus")
            self.item.frame = CGRect(x: itemPosition[Int.random(in: 0...1)], y: -self.item.frame.height, width: size.width, height: size.height)
            self.item.alpha = 0
            self.item.contentMode = .scaleAspectFit
            self.mainRoadView.addSubview(self.item)
            
            UIView.animate(withDuration: 1.5 - self.carSpeed, delay: 10, options: .curveLinear, animations: {
                self.item.alpha = 1
                self.item.frame.origin.y = self.mainRoadView.frame.height
            }) { (_) in
                self.item.removeFromSuperview()
                self.createAbilityItem()
            }
        }
    }
    
    @objc func checkCollision() {
        guard let playerPosition = self.car.layer.presentation()?.frame,
              let trafficPosition = self.traffic.layer.presentation()?.frame else { return }
        switch self.lightOn {
        case false:
           if playerPosition.intersects(trafficPosition) {
            self.endGame()
            }
        case true:
           break
        }
       }
       
       @objc func checkItem() {
        guard let playerPosition = self.car.layer.presentation()?.frame,
              let itemPosition = self.item.layer.presentation()?.frame else { return }
           
           if playerPosition.intersects(itemPosition) {
            self.turnOnLights()
            self.item.removeFromSuperview()
           }
       }
    
    @objc func increaseDifficulty() {
        self.carSpeed += 0.1
      }
    
    @objc func turnOnLights() {
        let frontLightSize = (width: self.car.frame.width * 3, height: self.car.frame.height * 2.5)
        let frontLightPosition = (x: CGFloat(0) - ((frontLightSize.width - car.frame.width) / 2), y: -frontLightSize.height)
        self.frontLight.image = UIImage(named: "frontLight")
        self.frontLight.frame = CGRect(x: frontLightPosition.x, y: frontLightPosition.y, width: frontLightSize.width, height: frontLightSize.height)
        self.frontLight.layer.cornerRadius = 20
        self.frontLight.alpha = 0
        self.car.addSubview(self.frontLight)
        
        let rearLightSize = (width: self.car.frame.width * 1.5, height: self.car.frame.width / 1.5)
        let rearLightPosition = (x: CGFloat(0) - ((rearLightSize.width - car.frame.width) / 2), y: self.car.frame.height)
        self.rearLight.image = UIImage(named: "rearLight")
        self.rearLight.frame = CGRect(x: rearLightPosition.x, y: rearLightPosition.y, width: rearLightSize.width, height: rearLightSize.height)
        self.rearLight.alpha = 0
        self.car.addSubview(self.rearLight)
        
        UIView.animate(withDuration: 0.3, delay: 0, animations: {
            self.rearLight.alpha = 0.5
            self.frontLight.alpha = 0.4
            self.changeColorRoad()
        }) { (_) in
            self.lightOn = true
            self.turnOffLights()
        }
    }
    
    @objc func turnOffLights() {
        if self.lightOn {
            UIView.animate(withDuration: 0.3, delay: 5, animations: {
                self.roadAnimationView.backgroundColor = .black
            }) { (_) in
                self.lightOn = false
                self.frontLight.removeFromSuperview()
                self.rearLight.removeFromSuperview()
            }
        }
        
    }
        
    @objc func createTurnSignal(signal: UIView) {
        if signal == self.leftTurnSignal {
            self.leftTurnSignal.image = UIImage(named: "turnLeft")
            self.leftTurnSignal.contentMode = .scaleAspectFit
            self.leftTurnSignal.frame = CGRect(x: 0, y: 0, width: car.frame.width, height: car.frame.height)
            car.addSubview(leftTurnSignal)
        }
        if signal == self.rightTurnSignal {
            self.rightTurnSignal.image = UIImage(named: "turnRight")
            rightTurnSignal.contentMode = .scaleAspectFit
            self.rightTurnSignal.frame = CGRect(x: 0, y: 0, width: self.car.frame.width, height: self.car.frame.height)
            self.car.addSubview(self.rightTurnSignal)
        }
    }
    
    @objc func moveRoad() {
        if self.startAnimation {
            self.road.image = UIImage(named: "neonRoad")
            self.road.frame = CGRect(x: self.roadAnimationView.bounds.origin.x, y: self.roadAnimationView.frame.origin.y, width: self.roadAnimationView.frame.width, height: self.roadAnimationView.frame.height)
            self.road.contentMode = .scaleToFill
            self.road.alpha = 0.7
            self.roadAnimationView.addSubview(road)
            
            UIView.animate(withDuration: 2 - self.carSpeed, delay: 0, options: .curveLinear, animations: {
                self.road.frame.origin.y = self.roadAnimationView.frame.height
            }) { (_) in
                self.road.removeFromSuperview()
                self.moveRoad()
            }
            self.moveSecondRoad()
        }
    }
    
    @objc func moveSecondRoad() {
        if self.startAnimation {
            self.secondRoad.image = UIImage(named: "neonRoad")
            self.secondRoad.frame = CGRect(x: self.roadAnimationView.bounds.origin.x, y: -self.roadAnimationView.frame.height, width: roadAnimationView.frame.width, height: roadAnimationView.frame.height)
            self.secondRoad.contentMode = .scaleToFill
            self.secondRoad.alpha = 0.7
            self.roadAnimationView.addSubview(secondRoad)
            
            UIView.animate(withDuration: 2 - self.carSpeed, delay: 0, options: .curveLinear, animations: {
                self.secondRoad.frame.origin.y = self.roadAnimationView.frame.origin.y
            }) { (_) in
                self.secondRoad.removeFromSuperview()
            }
        }
    }
    
    @objc func moveRoadMark() {
        if self.startAnimation {
            let roadMark = UIView()
            let roadMarkSize = (width: CGFloat(10), height: CGFloat(50))
            let roadMarkPosition = (x: (self.roadAnimationView.frame.width - roadMarkSize.width) / 2, y: CGFloat(0))
            
            roadMark.frame = CGRect(x: roadMarkPosition.x, y: roadMarkPosition.y, width: roadMarkSize.width, height: roadMarkSize.height)
            roadMark.backgroundColor = .white
            roadMark.alpha = 0.5
            roadMark.setRadiusWithShadow(radius: roadMark.frame.width / 2, color: UIColor.red.cgColor)
            self.roadAnimationView.addSubview(roadMark)
            
            UIView.animate(withDuration: 2 - self.carSpeed, delay: 0, options: .curveLinear, animations:  {
                roadMark.frame.origin.y = self.roadAnimationView.frame.height
            }) { (_) in
                roadMark.removeFromSuperview()
            }
        }
    }
    
    @objc func moveLeftBanners() {
        if self.startAnimation {
            let leftBanner = UIImageView()
            let size = self.leftRoadStripView.frame.width
            
            leftBanner.image = UIImage(named: "\(Int.random(in: (12...14)))")
            leftBanner.contentMode = .scaleToFill
            leftBanner.frame = CGRect(x: 0, y: -size * 1.5, width: size, height: size * 1.5)
            self.leftRoadStripView.addSubview(leftBanner)
            
            UIView.animate(withDuration: 2 - self.carSpeed, delay: Double.random(in: 0...3), options: .curveLinear, animations: {
                leftBanner.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                leftBanner.frame.origin.y = self.leftRoadStripView.frame.height
            }) { (_) in
                leftBanner.transform = CGAffineTransform(scaleX: 1, y: 1)
                leftBanner.removeFromSuperview()
                self.moveLeftBanners()
            }
        }
    }
    
    @objc func moveRightBanners() {
        if self.startAnimation {
            let rightBanner = UIImageView()
            let size = self.rightRoadStripView.frame.width
            
            rightBanner.image = UIImage(named: "\(Int.random(in: (15...17)))")
            rightBanner.contentMode = .scaleToFill
            rightBanner.frame = CGRect(x: 0, y: -size * 1.5, width: size, height: size * 1.5)
            self.rightRoadStripView.addSubview(rightBanner)
            
            UIView.animate(withDuration: 2 - self.carSpeed, delay: Double.random(in: 0...3), options: .curveLinear, animations: {
                rightBanner.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                rightBanner.frame.origin.y = self.rightRoadSide.frame.height
            }) { (_) in
                rightBanner.transform = CGAffineTransform(scaleX: 1, y: 1)
                rightBanner.removeFromSuperview()
                self.moveRightBanners()
            }
        }
    }
    
    @objc func animateLeftLight() {
        if self.startAnimation {
            let leftLight = UIView()
            let leftLightSize = (width: self.leftRoadSide.frame.width, height: leftRoadSide.frame.height)
            let leftLightPosition = (x: CGFloat(0), y: -leftLightSize.height)
            
            leftLight.frame = CGRect(x: leftLightPosition.x, y: leftLightPosition.y, width: leftLightSize.width, height: leftLightSize.height)
            leftLight.backgroundColor = .white
            leftLight.layer.cornerRadius = leftLight.frame.width / 2
            leftLight.alpha = 0.1
            self.leftRoadSide.addSubview(leftLight)
            
            UIView.animate(withDuration: 1.3 - self.carSpeed, delay: Double.random(in: 0...6), options: .curveLinear, animations:  {
                leftLight.frame.origin.y = self.mainRoadView.frame.height
            }) { (_) in
                leftLight.removeFromSuperview()
                self.animateLeftLight()
            }
        }
    }
    
    @objc func animateRightLight() {
        if self.startAnimation {
            let rightLight = UIView()
            let rightLightSize = (width: self.rightRoadSide.frame.width, height: self.rightRoadSide.frame.height)
            let rightLightPosition = (x: CGFloat(0), y: -rightLightSize.height)
            
            rightLight.frame = CGRect(x: rightLightPosition.x, y: rightLightPosition.y, width: rightLightSize.width, height: rightLightSize.height)
            rightLight.backgroundColor = .white
            rightLight.layer.cornerRadius = rightLight.frame.width / 2
            rightLight.alpha = 0.1
            self.rightRoadSide.addSubview(rightLight)
            
            UIView.animate(withDuration: 1.3 - self.carSpeed, delay: Double.random(in: 0...6), options: .curveLinear, animations:  {
                rightLight.frame.origin.y = self.mainRoadView.frame.height
            }) { (_) in
                rightLight.removeFromSuperview()
                self.animateRightLight()
            }
        }
    }
  
    private func showStartBanner() {

        self.raceBanner.image = UIImage(named: "raceBanner")
        let size = (width: self.view.frame.width, height: self.view.frame.height / 4)
        let position = (x: CGFloat(0), y: (self.view.frame.height - size.height) / 2 )
        self.raceBanner.frame = CGRect(x: position.x , y: position.y, width: size.width, height: size.height)
        self.raceBanner.addSubview(self.view)
        UIView.animate(withDuration: 2, animations:  {
            self.raceBanner.alpha = 0.7
            self.raceBanner.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }) { (_) in
            self.raceBanner.removeFromSuperview()
        }
    }
  
    private func changeColorRoad() {
        UIView.animate(withDuration: 2) {
            self.roadAnimationView.backgroundColor = .red
        }
    }
    @objc private func increaseScore() {
        self.score += 1
        self.scoreLabel.text = String(self.score)
    }
    
    func initGame() {
        self.sound.stopAudio()
        self.sound.playAudio(forResource: "race", ofType: "mp3", loops: -1)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleCar(_:)))
        self.view.addGestureRecognizer(tapRecognizer)
        
        self.createPlayerCar()
        self.createTraffic()
        self.createAbilityItem()
        
        self.showStartBanner()
    
        self.moveRoad()
        self.moveLeftBanners()
        self.moveRightBanners()
        
        self.animateLeftLight()
        self.animateRightLight()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.3 - self.carSpeed / 10, target: self, selector: #selector(self.moveRoadMark), userInfo: nil, repeats: true)
        self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.checkCollision), userInfo: nil, repeats: true)
        self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.checkItem), userInfo: nil, repeats: true)
        self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.increaseDifficulty), userInfo: nil, repeats: true)
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.increaseScore), userInfo: nil, repeats: true)
    }
    
    func nukeAllAnimations() {
        self.roadAnimationView.subviews.forEach({$0.layer.removeAllAnimations()})
        self.mainRoadView.subviews.forEach({$0.layer.removeAllAnimations()})
        self.leftRoadStripView.subviews.forEach({$0.layer.removeAllAnimations()})
        self.rightRoadStripView.subviews.forEach({$0.layer.removeAllAnimations()})
        self.rightRoadSide.subviews.forEach({$0.layer.removeAllAnimations()})
        self.leftRoadSide.subviews.forEach({$0.layer.removeAllAnimations()})
    }
    
   
    func endGame() {
        self.sound.stopAudio()
        self.sound.playAudio(forResource: "bang", ofType: "mp3", loops:  0)
        self.nukeAllAnimations()
        self.startAnimation = false
        self.carSpeed = 0
        self.traffic.removeFromSuperview()
        self.car.removeFromSuperview()
        
        self.timer.invalidate()
        
        var objects = ScoresManager.shared.loadScoreArray()
        guard let name = UserDefaults.standard.value(forKey: "name") as? String else { return }
        let result = Score(name: name, score: self.score, date: Date().getDate(), time: Date().getTime())
        objects.append(result)
        ScoresManager.shared.saveScoreArray(objects)
        
        guard let scores = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "ScoresViewController") as? ScoresViewController else { return
            print("Error switching to ScoresViewController")
        }
        self.navigationController?.pushViewController(scores, animated: true)

    }
    
}



