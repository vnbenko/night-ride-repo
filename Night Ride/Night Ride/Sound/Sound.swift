import AVFoundation

class Sound {
    
    var soundPlayer: AVAudioPlayer?
    
    func playAudio(forResource: String, ofType: String, loops: Int) {
        let path = Bundle.main.path(forResource: forResource, ofType: ofType)
        let url = URL(fileURLWithPath: path!)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            self.soundPlayer = try? AVAudioPlayer(contentsOf: url)
            if let sound = self.soundPlayer {
               
            sound.prepareToPlay()
            sound.numberOfLoops = loops
            sound.play() }
        } catch {
            // couldn't load file :(
        }
    }
    
    func stopAudio() {
        if let audioPlayer = self.soundPlayer {
            audioPlayer.stop()
        }
    }
}
