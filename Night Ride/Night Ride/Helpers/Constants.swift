import UIKit

class Constants {
    
    static let color = [UIColor.red.cgColor,
    UIColor.blue.cgColor,
    UIColor.white.cgColor,
    UIColor.yellow.cgColor,
    UIColor.orange.cgColor,
    UIColor.brown.cgColor,
    UIColor.purple.cgColor,
    UIColor.systemPink.cgColor,
    UIColor.green.cgColor]
    
    static let coralCar = UIImage(named: "coralCar")
    static let yellowCar = UIImage(named: "yellowCar")
    static let blackCar = UIImage(named: "blackCar")
    static let redCar = UIImage(named: "redCar")
    static let silverTraffic = UIImage(named: "silverTraffic")
    static let blackTraffic = UIImage(named: "blackTraffic")
    static let yellowTraffic = UIImage(named: "yellowTraffic")
    static let redTraffic = UIImage(named: "redTraffic")
}
