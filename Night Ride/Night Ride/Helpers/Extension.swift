import UIKit

extension UIView {
    
    func setRadiusWithShadow(radius: CGFloat? = nil, color: CGColor? = nil, position: CGSize? = nil, shadowVolume: CGFloat = 15, opacity: Float = 0.7) {
        self.layer.cornerRadius = radius ?? self.frame.width / 2
        self.layer.shadowColor = color ?? UIColor.black.cgColor
        self.layer.shadowOffset = position ?? CGSize(width: 0, height: 0)
        self.layer.shadowRadius = shadowVolume
        self.layer.shadowOpacity = opacity
        self.layer.masksToBounds = false
    }
    
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

extension Date {
    func getDate(format: String = "MM.dd.yy") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func getTime(format: String = "HH:mm") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

