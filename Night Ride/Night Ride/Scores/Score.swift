import UIKit

class Score: Codable {
    
    private var name: String
    private var score: Int
    private var date: String
    private var time: String
    
    enum CodingKeys: String, CodingKey {
        case name, score, date, time
    }
    
    init (name: String, score: Int, date: String, time: String) {
        self.name = name
        self.score = score
        self.date = date
        self.time = time
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try container.decode(String.self, forKey: .name)
        self.score = try container.decode(Int.self, forKey: .score)
        self.date = try container.decode(String.self, forKey: .date)
        self.time = try container.decode(String.self, forKey: .time)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.name, forKey: .name)
        try container.encode(self.score, forKey: .score)
        try container.encode(self.date, forKey: .date)
        try container.encode(self.time, forKey: .time)
    }
    
    func getName() -> String {
        return self.name
    }
    
    func getScore() -> Int {
        return self.score
    }
    
    func getDate() -> String {
        return self.date
    }
    
    func getTime() -> String {
        return self.time
    }
}
