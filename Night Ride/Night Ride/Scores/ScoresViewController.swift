import UIKit

class ScoresViewController: UIViewController {
    
    var scores = [Score]()
    var sound = Sound()
    
    @IBOutlet weak var scoresTableView: UITableView!
    @IBOutlet weak var yourScoresLabel: UILabel!
    @IBOutlet weak var headerNameLabel: UILabel!
    @IBOutlet weak var headerScoreLabel: UILabel!
    @IBOutlet weak var headerTimeLabel: UILabel!
    @IBOutlet weak var headerDateLabel: UILabel!
    @IBOutlet weak var scoresLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizeText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getScoresArray()
    }
    
    @IBAction func mainButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func raceButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        guard let race = self.storyboard?.instantiateViewController(identifier: "RaceViewController") as? RaceViewController else { return }
        
        self.navigationController?.pushViewController(race, animated: true)
    }
    
    
    private func getScoresArray() {
        let scores = ScoresManager.shared.loadScoreArray()
        self.scores = scores
    }

    private func localizeText() {
        self.yourScoresLabel.text = NSLocalizedString("Your scores", comment: "")
        self.headerNameLabel.text = NSLocalizedString("Name", comment: "")
        self.headerScoreLabel.text = NSLocalizedString("Score", comment: "")
        self.headerTimeLabel.text = NSLocalizedString("Time", comment: "")
        self.headerDateLabel.text = NSLocalizedString("Date", comment: "")
        self.scoresLabel.text = NSLocalizedString("Scores", comment: "")
        
    }

}

extension ScoresViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.scores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.scoresTableView.dequeueReusableCell(withIdentifier: "ScoresTableViewCell") as? ScoresTableViewCell else {
            return UITableViewCell()
        }
        let score = scores[indexPath.row]
        cell.configure(cell: cell, score: score)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = self.scoresTableView.frame.height / 10
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}
