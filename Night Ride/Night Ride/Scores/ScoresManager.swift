import UIKit

class ScoresManager {
    
    static let shared = ScoresManager()
    
    private init() {}
    
    private let defaults = UserDefaults.standard
    private let key = "scores"
    
    func loadScoreArray() -> [Score] {
        let array = self.defaults.value([Score].self, forKey: self.key)
        if var array = array {
            array.sort { $0.getScore() > $1.getScore() }
            return array
        }
        return []
    }
    
    func saveScoreArray(_ score: [Score]) {
        var array = self.loadScoreArray()
        array = score
        self.defaults.set(encodable: array, forKey: self.key)
    }

    func deleteScores(index: Int) {
        var array = self.loadScoreArray()
        array.remove(at: index)
        self.saveScoreArray(array)
    }
    
}

