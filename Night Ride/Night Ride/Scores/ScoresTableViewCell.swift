import UIKit

class ScoresTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(cell: ScoresTableViewCell, score: Score) {
        cell.nameLabel.text = score.getName()
        cell.scoreLabel.text = String(score.getScore())
        cell.dateLabel.text = score.getDate()
        cell.timeLabel.text = score.getTime()
    }
 
}
