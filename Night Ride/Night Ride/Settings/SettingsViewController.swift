import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var coralCarView: UIView!
    @IBOutlet weak var yellowCarView: UIView!
    @IBOutlet weak var blackCarView: UIView!
    @IBOutlet weak var redCarView: UIView!
    @IBOutlet weak var chooseCarView: UIView!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var difficultySegmentedControl: UISegmentedControl!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var chooseCarLabel: UILabel!
    @IBOutlet weak var selectDifficultyLabel: UILabel!
    @IBOutlet weak var changeNameButton: UIButton!
    @IBOutlet weak var settingsLabel: UILabel!
    
    
    var sound = Sound()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizeText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showName()
        self.showCurrentCar()
        self.showCurrentDifficulty()
    }
    
    
    @IBAction func coralCarButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        self.resetShadows()
        self.coralCarView.setRadiusWithShadow(color: UIColor.white.cgColor, opacity: 1)
        guard let image = Constants.coralCar else { return }
        SettingsManager.shared.setCar(image: image)
    }
    
    @IBAction func yellowCarButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        self.resetShadows()
        self.yellowCarView.setRadiusWithShadow(color: UIColor.white.cgColor, opacity: 1)
        guard let image = Constants.yellowCar else { return }
        SettingsManager.shared.setCar(image: image)
    }
    
    @IBAction func blackCarButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        self.resetShadows()
        self.blackCarView.setRadiusWithShadow(color: UIColor.white.cgColor, opacity: 1)
        guard let image = Constants.blackCar else { return }
        SettingsManager.shared.setCar(image: image)
    }
    
    @IBAction func redCarButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        self.resetShadows()
        self.redCarView.setRadiusWithShadow(color: UIColor.white.cgColor, opacity: 1)
        guard let image = Constants.redCar else { return }
        SettingsManager.shared.setCar(image: image)
    }
    
    @IBAction func difficultySegmentedControlPressed(_ sender: UISegmentedControl) {
        switch difficultySegmentedControl.selectedSegmentIndex {
        case 0:
            self.discriptionLabel.text = "Low traffic"
            self.discriptionLabel.text = NSLocalizedString("Low traffic", comment: "")
            self.difficultySegmentedControl.selectedSegmentTintColor = .green
            SettingsManager.shared.addTraffic(array: [Constants.silverTraffic!, Constants.redTraffic!])
        case 1:
            self.discriptionLabel.text = "Medium traffic"
            self.discriptionLabel.text = NSLocalizedString("Medium traffic", comment: "")
            self.difficultySegmentedControl.selectedSegmentTintColor = .yellow
            SettingsManager.shared.addTraffic(array: [Constants.silverTraffic!, Constants.redTraffic!, Constants.blackTraffic!])
        case 2:
            self.discriptionLabel.text = "High traffic"
            self.discriptionLabel.text = NSLocalizedString("High traffic", comment: "")
            self.difficultySegmentedControl.selectedSegmentTintColor = .red
            SettingsManager.shared.addTraffic(array: [Constants.silverTraffic!, Constants.redTraffic!, Constants.blackTraffic!, Constants.yellowTraffic!])
        default:
            break
        }
    }
    
    @IBAction func raceButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        guard let race = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "RaceViewController") as? RaceViewController else { return
        }
        
        self.navigationController?.pushViewController(race, animated: true)
    }
    
    @IBAction func mainButtonPressed(_ sender: UIButton) {
        self.sound.playAudio(forResource: "click", ofType: "mp3", loops: 0)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func changeNameButtonPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "Hi, friend!", message: "Do you want to change your name?", preferredStyle: .actionSheet)
        
        let changeNameAction = UIAlertAction(title: "Change name", style: .default) { _ in self.changeName() }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(changeNameAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func changeName() {
        let alert = UIAlertController(title: "Change name", message: "If you change your name, your results will be reset", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Player name"
        }
        
        let changeNameAction = UIAlertAction(title: "Change name", style: .default) { (_) in
            
            guard let name = alert.textFields?[0].text else { return }
            if name == "" {
                self.playerNameLabel.text = "Player 1"
            } else {
                self.playerNameLabel.text = name
                UserDefaults.standard.setValue(name, forKey: "name")
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(changeNameAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showName() {
        guard let name = UserDefaults.standard.value(forKey: "name") as? String else { return}
        if name == "" {
            self.playerNameLabel.text = "Player 1"
        } else {
            self.playerNameLabel.text = name
        }
    }
    
    private func showCurrentCar() {
        let car = SettingsManager.shared.getCar()
        switch car {
        case Constants.coralCar:
            self.resetShadows()
            self.coralCarView.setRadiusWithShadow(color: UIColor.white.cgColor, opacity: 1)
        case Constants.yellowCar:
            self.resetShadows()
            self.yellowCarView.setRadiusWithShadow(color: UIColor.white.cgColor, opacity: 1)
        case Constants.blackCar:
            self.resetShadows()
            self.blackCarView.setRadiusWithShadow(color: UIColor.white.cgColor, opacity: 1)
        case Constants.redCar:
            self.resetShadows()
            self.redCarView.setRadiusWithShadow(color: UIColor.white.cgColor, opacity: 1)
        default:
            self.resetShadows()
            self.coralCarView.setRadiusWithShadow(color: UIColor.white.cgColor, opacity: 1)
        }
    }
    
    private func showCurrentDifficulty() {
        let trafficArray = SettingsManager.shared.getTrafficArray()
        switch trafficArray.count {
        case 2:
            self.discriptionLabel.text = "Low traffic"
            self.discriptionLabel.text = NSLocalizedString("Low traffic", comment: "")
            self.difficultySegmentedControl.selectedSegmentIndex = 0
            self.difficultySegmentedControl.selectedSegmentTintColor = .green
        case 3:
            self.discriptionLabel.text = "Medium traffic"
            self.discriptionLabel.text = NSLocalizedString("Medium traffic", comment: "")
            self.difficultySegmentedControl.selectedSegmentIndex = 1
            self.difficultySegmentedControl.selectedSegmentTintColor = .yellow
        case 4:
            self.discriptionLabel.text = "High traffic"
            self.difficultySegmentedControl.selectedSegmentIndex = 2
            self.discriptionLabel.text = NSLocalizedString("High traffic", comment: "")
            self.difficultySegmentedControl.selectedSegmentTintColor = .red
        default:
            self.discriptionLabel.text = "Low traffic"
            self.discriptionLabel.text = NSLocalizedString("Low traffic", comment: "")
            self.difficultySegmentedControl.selectedSegmentIndex = 0
            self.difficultySegmentedControl.selectedSegmentTintColor = .green
        }
    }
    
    private func resetShadows() {
        self.chooseCarView.subviews.forEach({$0.setRadiusWithShadow(color: nil)})
    }
    
    private func localizeText() {
        self.chooseCarLabel.text = NSLocalizedString("Choose Your Car", comment: "")
        self.selectDifficultyLabel.text = NSLocalizedString("Select difficulty", comment: "")
        self.changeNameButton.setTitle("Change name".localized, for: .normal)
        self.settingsLabel.text = NSLocalizedString("Settings", comment: "")
    }
}

