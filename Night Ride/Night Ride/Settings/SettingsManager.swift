import UIKit

class SettingsManager {
    
    static let shared = SettingsManager()
    
    private init() {}
    
    private var car = Constants.coralCar
    private var trafficArray = [Constants.silverTraffic!, Constants.redTraffic!]
    
    func setCar(image: UIImage) {
        self.car = image
    }
    
    func getCar() -> UIImage {
        return self.car!
    }
    
    func addTraffic(array: [UIImage]) {
        self.trafficArray = array
    }
    
    func getTrafficArray() -> [UIImage] {
        return trafficArray
    }
    
    
}

